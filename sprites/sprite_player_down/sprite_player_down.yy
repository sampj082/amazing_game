{
    "id": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d36f5fd-09a9-4c44-b9c3-f2f2a38a256c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "58c4975d-e7a5-436f-af96-ff32276bfe89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d36f5fd-09a9-4c44-b9c3-f2f2a38a256c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501f50dc-59e6-4000-9629-81d059f072e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d36f5fd-09a9-4c44-b9c3-f2f2a38a256c",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "c06271ad-84cc-4c52-8ec8-0047b810e8dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "146d9ca0-3823-4c27-8b3e-7faef862bb30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c06271ad-84cc-4c52-8ec8-0047b810e8dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8102e56-65f5-40ae-b809-2cac2193a7db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c06271ad-84cc-4c52-8ec8-0047b810e8dd",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "4cc4c08f-a802-48ed-a382-2f9331928969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "56a2ab1a-5803-48b0-8591-aff11c38c8bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc4c08f-a802-48ed-a382-2f9331928969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb4bbfe-e1ad-46fc-83ca-91bcceda05c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc4c08f-a802-48ed-a382-2f9331928969",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "56bc256a-577a-4645-bdae-e6a1bd8ea7ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "a43f2930-b38d-466e-b78d-27393c8c972d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bc256a-577a-4645-bdae-e6a1bd8ea7ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0149b41-2160-4c79-97c6-262b8a2bf461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bc256a-577a-4645-bdae-e6a1bd8ea7ae",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "13303d4a-788c-4926-aed3-7d761fbaafc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "189d8f73-d95e-42ae-99a4-032d7fb67c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13303d4a-788c-4926-aed3-7d761fbaafc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e024855-de8c-4805-9dbe-65e3f35d2c73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13303d4a-788c-4926-aed3-7d761fbaafc8",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "e596c3e6-259d-49e6-a79d-354930d2edbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "336e3987-09e4-41f2-8bad-e2bfaa8bdd5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e596c3e6-259d-49e6-a79d-354930d2edbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af13bbd5-af85-40f1-b981-aa6794e696e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e596c3e6-259d-49e6-a79d-354930d2edbf",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "85703366-98b8-4646-95b8-342a14201baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "8177a6ce-55ba-4b5f-9769-649372fe63b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85703366-98b8-4646-95b8-342a14201baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbd3a3d-dae4-4ed0-84b5-b199e94993aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85703366-98b8-4646-95b8-342a14201baf",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        },
        {
            "id": "5626b3e9-7089-4d19-bca2-d2313a941e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "compositeImage": {
                "id": "dad8efd3-6e6e-4159-ad4b-92de72d0950b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5626b3e9-7089-4d19-bca2-d2313a941e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cc5558-0d50-4f6d-8984-b0a0f0db17d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5626b3e9-7089-4d19-bca2-d2313a941e07",
                    "LayerId": "6f05e74f-078e-4a24-b2a5-26d5c86781ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f05e74f-078e-4a24-b2a5-26d5c86781ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3630ab98-9d90-40d8-8ce0-4f5abe8f08b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}